# Scrubit

A simple application to scrub sensitive data from JSON input

Get Repo:
```terminal 
git clone https://gitlab.com/Cifranic/scrubit.git && cd ./scrubit
```

Run Application:
```terminal
python3 scrubit.py
```

## In docker:

Navigate to repo root (if not already)
```terminal
docker build -t scrubit .

docker run -it scrub
```

![Expected Output](./cap/shot.png)
