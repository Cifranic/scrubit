#Program Name:  scrubit.py
#Purpose: Scrub sensitive information from JSON file
#Author: Nicholas M. Cifranic
#Date: 12/22/20

# Assumptions: 
#  - JSON data incoming will be structured like the example input provided
#  - argument provided is not to be modified (sample.json)
#  - boolean and null values do not need to be scrubbed
#  - numbers are converted to strings before they are scrubbed

# Imports
import json
import re

# Type: Function
# Purpose: JSON validator
# Parameters: json as string
# Returns: True if json is valid
def jsonLint(j):
    try:
        json_object = json.loads(j)
    except ValueError as e:
        return False
    return True

# Type: Function
# Purpose: censor a string
# Parameters: string
# Returns: a string with all characters and numbers displayed as string
#   - Excludes:  Whitespace, special characters
def starify(s):

    # ensure input is a string 
    s = str(s)

    # variable string to hold new star value
    starVal = ''

    # filters 
    filters = "!@#$%^&*()_+-=[],. "

    # treat string as array, and itterate over each character
    for i in s:
                
        if i not in filters:
            # if number or valid upper/lower character, replace with star
            starVal += "*"
        else:
            # if whitespace, or special character, leave it.
            starVal += i

    return starVal



# Type: Function
# Purpose
#  - Take in valid JSON
#  - Parse it
#  - Scrub sensitive fields 
# Returns: String, in JSON format with srubbed data blocked with '*' symbol 
def srub(j):

    
    # convert to json object
    parsed = json.loads(j)

    # itterate through objects 
    for i in parsed:
        
        
        # if stand-alone int, dont worry about sanitizing
        if isinstance(parsed[i], int):
            continue   

        # if standalone string
        elif isinstance(parsed[i], str):
            
            # replace value with stars 
            parsed[i] = starify(parsed[i])

        # if list
        elif isinstance(parsed[i], list):

            # a temp list to hold star values 
            newL = []

            # itterate through 
            for j in parsed[i]:
                
                # append star values to list
                newL.append(starify(j))
            
            # assgin "stared" list to parsed[i]
            parsed[i] = newL
         
        # if dictonary                
        elif isinstance(parsed[i], dict):
            
            # itterate through dictonary. 
            for j in parsed[i]:
                if j != "expiration":
                    # add stared value to dictonary key, if it's not expiration
                    parsed[i][j] = starify(parsed[i][j])
                else:
                    continue
                                             
                                                 
    return parsed

# Main
if __name__ == '__main__':

    # example json as string
    jsonInput="""
    {
        "id": 121948,
        "name": "John Doe",
        "phone": "(555) 555-5555",
        "username": "john1234",
        "email": [
            "john@doe.com",
            "jdoe@hotmail.com",
            "j.dizzle@alumni.sarahlawrence.edu"
        ],
        "credit_card": {
            "card_number": "4111 1111 1111 1111",
            "expiration": "01/15",
            "name": "John X. Doe",
            "cvc": "123"
        }
    }
    """

    # Only proceed if json input is valid
    #print("Checking if JSON is valid...")
    if jsonLint(jsonInput):

        #print("Json is valid\n")

        # send json to scrub function
        print(srub(jsonInput))
       
    
    else:
        # input is not valid json
        print("Input not valid JSON!") 
